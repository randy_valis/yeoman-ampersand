var generators = require('yeoman-generator');
var mkdirp = require('mkdirp');

module.exports = generators.Base.extend({
    init: function(){

    },
    prompting: function () {
        var done = this.async();
        this.prompt({
          type    : 'input',
          name    : 'name',
          message : 'Your project name',
          default : this.appname
        }, function (answers) {

          this.projectName = answers.name;
          done();

        }.bind(this));
    },
    copyDirs: function(){
        /**
         * Copy all of the template stuff to the app directory
         */
        this.directory(
          this.templatePath('client'),
          this.destinationPath(this.projectName + '/client')
        );

        this.directory(
          this.templatePath('public'),
          this.destinationPath(this.projectName + '/public')
        );

        this.directory(
          this.templatePath('stylesheets'),
          this.destinationPath(this.projectName + '/stylesheets')
        );

        this.directory(
          this.templatePath('config'),
          this.destinationPath(this.projectName + '/config')
        );

        this.directory(
          this.templatePath('templates'),
          this.destinationPath(this.projectName + '/templates')
        );

        this.directory(
          this.templatePath('test'),
          this.destinationPath(this.projectName + '/test')
        );

        this.copy(this.templatePath('fakeApi.js'), this.projectName + '/fakeApi.js');
        this.copy(this.templatePath('server.js'), this.projectName + '/server.js');
        this.copy(this.templatePath('staticRoutes.js'), this.projectName + '/staticRoutes.js');
        this.copy(this.templatePath('webpack.config.js'), this.projectName + '/webpack.config.js');

        // empty dirs
        mkdirp.sync(this.projectName + '/client/helpers');
        mkdirp.sync(this.projectName + '/client/forms');
        mkdirp.sync(this.projectName + '/client/lib');

    },
    makePackageJSON: function(){
        var done = this.async();
        this.prompt({
          type    : 'input',
          name    : 'author',
          message : 'Your name',
        }, function (answers) {

            this.fs.copyTpl(
                this.templatePath('_package.json'),
                this.destinationPath(this.projectName + '/package.json'),
                {
                    author: answers.author,
                    projectName: this.projectName
                }
            );
            done();

        }.bind(this));

    }
});
