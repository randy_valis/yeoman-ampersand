module.exports = {
    entry: "./client/app.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            // { test: /\.css$/, loader: "style!css" },
            // { test: /\.jade$/, loader: "jade" },
            // { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' }
        ]
    },
    node: {
      fs: "empty"
    }
};
