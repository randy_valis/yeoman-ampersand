var Hapi = require('hapi');
var config = require('getconfig');
var fakeApi = require('./fakeApi');
var staticRoutes = require('./staticRoutes');
var internals = {};

var server = new Hapi.Server();
server.connection({ host: config.http.listen, port: config.http.port });

// set clientconfig cookie
internals.configStateConfig = {
    encoding: 'none',
    ttl: 1000 * 60 * 15,
    isSecure: config.isSecure
};
server.state('config', internals.configStateConfig);
internals.clientConfig = JSON.stringify(config.client);
// server.ext('onPreResponse', function(request, reply) {
//     if (!request.state.config) {
//         var response = request.response;
//         return reply(response.state('config', encodeURIComponent(internals.clientConfig)));
//     }
//
//     return reply.continue();
// });

// serve the js bundle
server.route({
    method: 'GET',
    path: '/bundle.js',
    handler: {
        file: 'bundle.js'
    }
});

// server out styles
server.route({
    method: 'GET',
    path: '/stylesheets/{p*}',
    handler: {
        directory: {
            path: 'stylesheets'
        }
    }
});

// serve the index page
server.route({
    method: 'GET',
    path: '/{path?}',
    handler: {
        file: 'index.html'
    }
});

// require moonboots_hapi plugin
server.register([
    {
        register: fakeApi.register
    },
    {
        register: staticRoutes
    }
], function (err) {
    if (err) throw err;
    // If everything loaded correctly, start the server:
    server.start(function (err) {
        if (err) throw err;
        console.log('Ampersand Test App is running at: http://' + config.http.listen + ':' + config.http.port);
    });
});
